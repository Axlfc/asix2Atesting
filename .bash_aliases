#!/bin/bash
if [ -f /.config/user-dirs.dirs ]; then
    source ${HOME}/.config/user-dirs.dirs
fi

# Testing aliases from Customizer
alias CUSTOMIZER="cd ~/Escritorio/git/Linux-Auto-Customizer"
alias ASIX="cd ~/Escritorio/git/gitlab/asix2Atesting"
alias cd..=".cd .."
alias c:="cd .."
alias i="bash install.sh -v"
alias io="bash install.sh -v -o"
alias oi="bash install.sh -v -o"
alias giff="git diff"
alias tconf="edit tmuxconf"
alias sshconf="edit sshconf"

#editing
alias editalias="edit aliases"
alias editallbashrc="edit allbashrc"
alias editbashrc="edit bashrc"
alias editbashfunctions="edit bashfunctions"
alias editfavorites="edit favorites"
alias editgrub="sudo pluma /etc/default/grub"
alias editinitializations="edit initializations"
alias editmime="edit mime"
alias editprofile="edit profile"

# System

#Operation
alias cow="cowsay"
alias fancy="figlet"
alias life='lolcat -a -d 25'
alias tuxsay='cowsay -f tux '
alias path="echo -e ${PATH//:/\\n}"
