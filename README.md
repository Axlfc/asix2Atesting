# asix2Atesting

######Brief explanation
You can interact with the mouse on the terminal, click, drag and session management.

This tools are meant for programmers who want to center only to write so much faster.

Use the command to paste from outside to the terminal:

`Ctrl+Shift+v`

`select` with mouse text to copy from terminal (text will be seen outmarked yellow)

To copy select dragging the desired text to copy from terminal copied with `right left`, you can copy with `right click` or only selecting it will be copied from the terminal.


Use `middle mouse click` to paste text into the terminal,
and regular `Paste` to its own to out of the terminal.

To open multiplexer window menu `right click` on the terminal screen.

##Terminal keystrokes & mnemonics
Key combinations

| Navigation | Terminal behaviour |
|-------------|----------------------|
|`Ctrl+a`| our favorite terminal keystroke, for itself it does nothing|
|`Ctrl+a` `&`| Kill terminal window|
|`Ctrl+a` `$`| Rename terminal session|
|`Ctrl+a` `,` | rename window | 
|`Ctrl+a` `-` | split terminal horizontally |
|`Ctrl+a` &#124;| split terminal vertically |
|`Ctrl+a` ` `| Reorder terminal sessions |
|`Ctrl+a` `:`| Prompt |
|`Ctrl+a` `;`| go to the 'last' (previously used) pane |
|`Ctrl+a` `c` | create session |
|`Ctrl+a` `d`| detatch terminal session|
|`Ctrl+a` `h`| left |
|`Ctrl+a` `j`| down |
|`Ctrl+a` `k`| up | 
|`Ctrl+a` `l`| right|
|`Ctrl+a` `n` | next window |
|`Ctrl+a` `o` | swap panes |
|`Ctrl+a` `p` | previous window |
|`Ctrl+a` `q` | show pane numbers |
|`Ctrl+a` `f` | find window |
|`Ctrl+a` `r` | reload terminal configuration |
|`Ctrl+a` `s` | list terminal sessions |
|`Ctrl+a` `S` | swap window |
|`Ctrl+a` `w` | new window |
|`Ctrl+a` `x` | kill pane |
|`Ctrl+a` `z`| zoom in/out |
|`Ctrl+a` `Arrow Keys`| Move to pane |
|`Ctrl+a` `?` | Help |

##Session management of terminal via command
Simply write in the terminal and hit `Enter`, it is possible that you need to be out of a tmux session before
typing some of the following session management commands.

| Name | Description |
|-------------|----------------------|
| `oldt` | Creates a new `default` `tmux` session and attaches to it |
| `dt` | Creates `tmux` session dettached |
| `ta` | Terminal `tmux` attach to session |
| `td` | Dettach from `tmux`session |
| `ts` | Terminal `tmux` switch from session |
| `tl` | Lists `tmux` sessions  |
| `tn` | Create new `tmux` session  |
| `tls` | List current running `tmux` sessions simpler|
| `tsk` | Kill certain running `tmux` session/s |
| `tks` | Kill running `tmux` sessions server|

## System functions installed from Linux-Auto-Customizer
| Name | What function does |
|-------------|----------------------|
| `b` | An alias to `bash` for Linux system developers who are constantly writing `bash` for testing |
| `c` | It does work very much like a regular cd, but it also is a `clear` alias when launched itself |
| `commit "`content of commit`"` | Stands for `git commit -am "`content of commit`"`|
| `e` | Multi Function `e` to edit a file or project in folder  |
| `l` | Special alias for `ls` |
| `L` | More extended version of `ls` |
| `o` | Opens folder `o` typed in a terminal display directory using `nemo` |
| `status` | Stands for `git status` |
| `fetch` | Stands for `git fetch` |
| `gitk` | Stands for `gitk --all --date-order &`

# Main system aliases
| Name | What function does |
|-------------|----------------------|
| `a` | Stands for `alias`, shows all current aliases |

#####It is so possible the terminal suggests to install other packages to use the own aliases, only install if the package is interesting to you or make a previous research.
##Installing environment preparation

`sudo apt-get install -y git`

`mkdir ${HOME}/Desktop/git`

`cd ${HOME}/Desktop/git`

`git clone https://github.com/AleixMT/Linux-Auto-Customizer`

####(optional but recommended)

`bash Linux-Auto-Customizer/install.sh -o --c --e --o --history-optimization --prompt --shortcuts --l-alias -L --templates --nemo`

####(next commands are the necessary to install asix2Atesting)

`git clone https://github.com/dracula/gnome-terminal`

`bash gnome-terminal/install.sh`

`sudo bash Linux-Auto-Customizer/install.sh -o --gnome-terminal --git --tmux`

`sudo apt-get install -y xdotool xclip tmuxp`

`git clone https://gitlab.com/Axlfc/asix2Atesting`

`cd asix2Atesting`

`sudo bash install.sh`

`chmod +x ~/.tmux_path.sh`

###Add following lines in `~/.bashrc` if not added:

list=("/home/axel/.bash_aliases" "/home/axel/.tmux_features " "/home/axel/.tmux_path" "/etc/bash_completion.d/ta" "/etc/bash_completion.d/td" "/etc/bash_completion.d/ts" "/etc/bash_completion.d/tsk" "/home/axel/.bin/bash-functions/.bash_functions")

for item in ${list[@]}; do
  if [ -f "$item" ]; then
    source "$item"
  fi
done

###To not always opening `tmux` when a terminal starts remove the following from `~/.tmux_features`:

if [ -z "$TMUX" ]; then

    attach_session=$(tmux 2> /dev/null ls -F \
        '#{session_attached} #{?#{==:#{session_last_attached},},1,#{session_last_attached}} #{session_id}' |
        awk '/^0/ { if ($2 > t) { t = $2; s = $3 } }; END { if (s) printf "%s", s }')

    if [ -n "$attach_session" ]; then
        tmux attach -t "$attach_session"
    else
        tmux
    fi
fi

# Uninstalling

`sudo apt-get purge -y xdotool xclip tmuxp`

`sudo bash ${HOME}/Desktop/git/Linux-Auto-Customizer/uninstall.sh --c --e --o --history-optimization --prompt --shortcuts --l-alias -L --templates --nemo`

`cd ${HOME}/Desktop/git/asix2Atesting`

`sudo bash uninstall.sh`

`rm -Rf ${HOME}/Desktop/git*`



