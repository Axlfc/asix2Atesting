#!/usr/bin/env bash
install_program()
{
  file_name=$(echo "$1" | rev | cut -d "/" -f1 | rev)
  if [[ -f "$file_name" ]]; then
    cp -p "${DIR}/${file_name}" "$1"
    chmod 751 "$1"
  fi
}

main()
{
  programfiles=("/home/${SUDO_USER}/.bash_aliases"
    "/home/${SUDO_USER}/.tmux.conf"
    "/home/${SUDO_USER}/.tmux_features"
    "/home/${SUDO_USER}/.tmux_path.sh" 
    "/etc/bash_completion.d/ta"
    "/etc/bash_completion.d/td" 
    "/etc/bash_completion.d/ts" 
    "/etc/bash_completion.d/tsk")

  if [[ "$(whoami)" != "root" ]]; then
    echo "ERROR: you need to be root"
    exit 1
  else
    # We perform a secure backup of previous files with same name
    for program in "${programfiles[@]}"; do
      cp -p "${program}" "${program}.bak"
    done
    for program in "${programfiles[@]}"; do
    	install_program "${program}"
    done
    if [[ -f "/home/${SUDO_USER}/.bash_functions" ]]; then
      #list=("/home/axel/.bash_aliases" "/home/axel/.tmux_features" "/home/axel/.tmux_path" "/etc/bash_completion.d/ta" "/etc/bash_completion.d/td" "/etc/bash_completion.d/ts" "/etc/bash_completion.d/tsk" "/home/axel/.bin/bash-functions/.bash_functions")

#for item in ${list[@]}; do
  #if [ -f "$item" ]; then
    #source "$item"
  #fi
#done
      echo "source /home/${SUDO_USER}/.bash_functions" >> "/home/${SUDO_USER}/.bashrc"
	
    fi
  fi
}




DIR="${BASH_SOURCE%/*}"
if [[ ! -d "${DIR}" ]]; then
  DIR="${PWD}"
fi


main "%@"
